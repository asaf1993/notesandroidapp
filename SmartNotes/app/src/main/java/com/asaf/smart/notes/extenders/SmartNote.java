package com.asaf.smart.notes.extenders;


import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.asaf.smart.notes.utils.FileUtil;
import com.asaf.smart.notes.utils.JsonUtil;


public class SmartNote {

    public static String jsonKeyNoteId = "note_id";
    public static String jsonKeySubject = "note_subject";
    public static String jsonKeyBody = "note_body";
    public Integer noteId;
    public String noteSubject;
    public String noteBody; //TODO: Add body class for images/text
    public Date modificationDate;
    public boolean isPined = false;
    public final static String notesFile = "smartnotes.dat";

    private final static String LOG_TAG = "SmartNoteObject";

    public FileUtil fileUtil;
    public JsonUtil jsonUtil;

    //Constructors:
    public SmartNote(){
        //Empty args constructor
    }

    public SmartNote(String jsonString){
        jsonUtil = new JsonUtil(jsonString);

        noteId = jsonUtil.getJsonIntValueByKey(SmartNote.jsonKeyNoteId);
        noteSubject = jsonUtil.getJsonStringValueByKey(SmartNote.jsonKeySubject);
        noteBody = jsonUtil.getJsonStringValueByKey(SmartNote.jsonKeyBody);
        //Empty args constructor
    }

    public SmartNote(String subject, String body){
        //args constructor for new note
        noteSubject = subject;
        noteBody = body;
    }

    public SmartNote(Integer id, String subject, String body){
        //args constructor for old note
        noteId = id;
        noteSubject = subject;
        noteBody = body;
        //modificationDate = ; TODO: fix when adding modify timestamp
    }


    //Methods:
    public void setNoteSubject(String newSubject){
        updateModificationDate();
        noteSubject = newSubject;
    }

    public void setNoteBody(String newBody){
        updateModificationDate();
        noteBody = newBody;
    }

    public Integer getNoteId(){
        return noteId;
    }

    public String getNoteSubject(){
        return noteSubject;
    }

    public String getNoteBody(){
        return noteBody;
    }

    public Date getNoteLastModificationDate() {
        return modificationDate;
    }

    private void updateModificationDate(){
        this.modificationDate = new Date();
    }

    @Override
    public String toString(){
        return this.getNoteJsonString();
    }

    private String getNoteJsonString(){

        jsonUtil = new JsonUtil();

        jsonUtil.addToJson(SmartNote.jsonKeyNoteId, noteId);
        jsonUtil.addToJson(SmartNote.jsonKeySubject, noteSubject);
        jsonUtil.addToJson(SmartNote.jsonKeyBody, noteBody);
        //jsonUtil.addToJson("modify_date", modificationDate);

        return jsonUtil.getJsonToString();
    }

    private static ArrayList<String> getNotesJsonStringsArray( ArrayList<SmartNote> smartNoteArrayList){

        ArrayList<String> strings = new ArrayList<>();

        for (SmartNote note : smartNoteArrayList){
            JsonUtil jsonUtil = new JsonUtil();

            jsonUtil.addToJson(SmartNote.jsonKeyNoteId, note.noteId);
            jsonUtil.addToJson(SmartNote.jsonKeySubject, note.noteSubject);
            jsonUtil.addToJson(SmartNote.jsonKeyBody, note.noteBody);
            //jsonUtil.addToJson("modify_date", modificationDate);

            strings.add(jsonUtil.getJsonToString());
        }

        return strings;
    }

    public static void deleteNote(Context ctx, Integer Id){

        JsonUtil jsonUtil;
        FileUtil fileUtil  = new FileUtil(ctx);

        ArrayList<String> notesFileLines = fileUtil.readFileLines(notesFile);
        ArrayList<String> notesFileLinesSuffix = new ArrayList<>();

        for(String lineString : notesFileLines){
            jsonUtil = new JsonUtil(lineString);
            Integer currentId = jsonUtil.getJsonIntValueByKey(SmartNote.jsonKeyNoteId);
            //Log.d(LOG_TAG, "Current ID: " + currentId.toString() + " Searching: " + Id.toString());

            if( !currentId.equals(Id) ){
                notesFileLinesSuffix.add(lineString);
            }
        }
        fileUtil.writeFileLines(notesFile, notesFileLinesSuffix);
    }

    private void saveNewNoteToFile(Context ctx){

        String noteJsonString;
        String lineSuffix;

        fileUtil  = new FileUtil(ctx);
        ArrayList<String> currentNotesLines = fileUtil.readFileLines(notesFile);

        noteJsonString = getNoteJsonString();
        lineSuffix = noteJsonString + "\n";

        currentNotesLines.add(lineSuffix);

        fileUtil.writeFileLines(notesFile, currentNotesLines);
    }

    private void updateNoteInFile(Context ctx){

        ArrayList<SmartNote> smartNotes = SmartNote.getSavedNotes(ctx);

        for (SmartNote note : smartNotes){

            if(note.noteId.equals(noteId)){

                note.setNoteSubject(noteSubject);
                note.setNoteBody(noteBody);
                break;
            }
        }
        setNewNotes(ctx, smartNotes);
    }

    public int saveNote(Context ctx){

        updateModificationDate();

        Boolean isNewNote = false;

        if(noteId == null) {
            this.noteId = getNewNoteId(ctx);
            isNewNote = true;
        }

        if(isNewNote){
            saveNewNoteToFile(ctx);
        }
        else{
            updateNoteInFile(ctx);
        }

        return noteId;
    }


    public Integer getNewNoteId(Context ctx){

        Integer newNoteId;

        ArrayList<Integer> smartNoteIdsArrayList = getSavedNotesIds(ctx);

        if (smartNoteIdsArrayList.isEmpty()){
            return 0;
        }

        newNoteId = Collections.max(smartNoteIdsArrayList) + 1;
        return newNoteId;
    }

    private ArrayList<Integer> getSavedNotesIds(Context ctx){

        ArrayList<SmartNote> smartNoteArrayList = getSavedNotes(ctx);
        ArrayList<Integer> smartNoteIdsArrayList = new ArrayList<>();

        if (smartNoteArrayList.isEmpty()){
            return smartNoteIdsArrayList;
        }

        for (SmartNote smartNote : smartNoteArrayList){
            smartNoteIdsArrayList.add(smartNote.noteId);
        }

        return smartNoteIdsArrayList;
    }

    public static ArrayList<SmartNote> getSavedNotes(Context ctx){

        SmartNote smartNote;
        ArrayList<SmartNote> smartNotesArray = new ArrayList<>();

        FileUtil fileUtil = new FileUtil(ctx);
        JsonUtil jsnUtil;
        ArrayList<String> fileTextLines = fileUtil.readFileLines(notesFile);

        if (fileTextLines.isEmpty()){
            return smartNotesArray;
        }

        for (String jsonString : fileTextLines) {

            jsnUtil = new JsonUtil(jsonString);

            Integer noteId = jsnUtil.getJsonIntValueByKey(SmartNote.jsonKeyNoteId);
            String noteSubject = jsnUtil.getJsonStringValueByKey(SmartNote.jsonKeySubject);
            String noteBody = jsnUtil.getJsonStringValueByKey(SmartNote.jsonKeyBody);
            //Date noteModifyDate = jsonUtil.getJsonStringValueByKey("modify_date"); //TODO: fix when adding modify timestamp

            smartNote = new SmartNote(noteId, noteSubject, noteBody);
            //Log.d(LOG_TAG, "Got Note: " +noteId.toString() +" "+ noteSubject +" "+ noteBody);
            smartNotesArray.add(smartNote);
        }

        return smartNotesArray;
    }

    public static void setNewNotes(Context ctx, ArrayList<SmartNote> newOrderedNotes){

        FileUtil flUtil = new FileUtil(ctx);
        ArrayList<String> newWriteLines = SmartNote.getNotesJsonStringsArray(newOrderedNotes);
        flUtil.writeFileLines(notesFile, newWriteLines);
    }
}
