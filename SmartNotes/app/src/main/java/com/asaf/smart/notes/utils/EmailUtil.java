package com.asaf.smart.notes.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.asaf.smart.notes.props.EmailProps;
import com.sendgrid.*;

import org.json.JSONException;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class EmailUtil {

    private static final String LOG_TAG = "EmailsUtil";
    private SendGrid sendGrid;
    private SendGrid.Email email;
    private Boolean toSet = false;
    private Boolean subjectSet = false;
    private Boolean bodySet = false;
    private Boolean sent = false;


    public EmailUtil() {
        sendGrid = new SendGrid(EmailProps.mailApiKey);
        email = new SendGrid.Email();
    }


    public void setToEmail(String email_address){
        email.addTo(email_address);
        toSet = true;
    }

    public void setSubject(String subject) {
        email.setSubject(subject);
        subjectSet = true;
    }

    public void setBody(String body) {
        email.setText(body);
        bodySet = true;
    }

    public void setTemplate(String Id){

        try{
            email.setTemplateId(Id);
        }
        catch (JSONException ex){
            Log.d(LOG_TAG, "Could not set template: " + ex.toString());
        }
    }

    public void addAttachment(String fileName, File file) {

        try {
            email.addAttachment(fileName, file);
        }
        catch(Exception exception){
            Log.d(LOG_TAG, "Failed to add attachment to email: " + exception.toString());
        }
    }

    public void addAttachment(String fileName, String fileText) {

        try {
            email.addAttachment(fileName, fileText);
        }
        catch(Exception exception){
            Log.d(LOG_TAG, "Failed to add attachment to email: " + exception.toString());
        }
    }


    public Boolean sendEmail() {

        if (!toSet | !subjectSet | !bodySet){
            Log.d(LOG_TAG, "Email not sent, mandatory params not set: " + toSet + subjectSet + bodySet);
        }

        try {
            EmailAsyncTask emailAsyncTask = new EmailAsyncTask();
            emailAsyncTask.execute().get(EmailProps.mailTimeOutMilliSec, TimeUnit.MILLISECONDS);
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Email not sent: " + e.toString());
        }

        return sent;
    }

    private class EmailAsyncTask extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... voids) {

            email.setFrom(EmailProps.mailFromSupport);

            try {
                sendGrid.send(email);
            } catch (SendGridException e) {

                Log.d(EmailUtil.LOG_TAG, "SendGrid failed: " + e);
                return false;
            }

            Log.d(LOG_TAG, "Email Sent");
            sent = true;
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //Post Execute result

            Log.d(LOG_TAG, "Email post execute result: " + result);
        }
    }
}
