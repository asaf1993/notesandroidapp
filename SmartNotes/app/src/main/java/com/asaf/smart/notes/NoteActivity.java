package com.asaf.smart.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.asaf.smart.notes.extenders.LinedEditText;
import com.asaf.smart.notes.extenders.NotesRecyclerAdapter;
import com.asaf.smart.notes.extenders.SmartNote;


public class NoteActivity extends Activity {

    private final static String LOG_TAG = "NoteActivity";
    private SmartNote smartNote = new SmartNote();
    private boolean isOldNote = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_note);

        EditText editSubject = (EditText) findViewById(R.id.note_title);
        EditText editBody = (EditText) findViewById(R.id.note_body);

        try{
            Intent intent = getIntent();
            String noteJson = intent.getStringExtra(NotesRecyclerAdapter.EXTRA_EDIT_NOTE);

            if(!noteJson.isEmpty()){
                //Log.d(LOG_TAG, noteJson);
                smartNote = new SmartNote(noteJson);

                editSubject.setText(smartNote.getNoteSubject());
                editBody.setText(smartNote.getNoteBody());

                isOldNote = true;

                Log.d(LOG_TAG, "Old note activity");
            }
            else {
                Log.d(LOG_TAG, "New note activity");
            }
        }
        catch (Exception e){
            Log.d(LOG_TAG, "Exception:" + e.toString());
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        hideKeyboard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void deleteNoteButton(MenuItem item){

        SmartNote.deleteNote(getApplicationContext(), smartNote.getNoteId());
        Toast.makeText(getApplicationContext(), R.string.note_deleted_msg, Toast.LENGTH_SHORT).show();
        this.finish();
    }

    public void saveNoteButton(MenuItem item){

        EditText subjectEdit = (EditText) findViewById(R.id.note_title);
        String subjectText = subjectEdit.getText().toString();

        LinedEditText bodyEdit = (LinedEditText) findViewById(R.id.note_body);
        String bodyText = bodyEdit.getText().toString();

        Context context = getApplicationContext();

        if(subjectText.isEmpty()) {
            Toast.makeText(context, R.string.note_empty_subject_msg, Toast.LENGTH_SHORT).show();
        }
        else if(bodyText.isEmpty()) {
            Toast.makeText(context, R.string.note_empty_body_msg, Toast.LENGTH_SHORT).show();
        }
        else {

            saveNoteGetId(subjectText, bodyText);
            Toast.makeText(context, R.string.note_saved_msg, Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }

    public int saveNoteGetId(String noteSubject, String noteBody){

        int smartNoteId; //New note generated ID
        Context ctx = getApplicationContext();

        if (isOldNote) {
            smartNote.setNoteSubject(noteSubject);
            smartNote.setNoteBody(noteBody);
            smartNoteId = smartNote.saveNote(ctx);
        }

        else {
            smartNote = new SmartNote(noteSubject, noteBody);
            smartNoteId = smartNote.saveNote(ctx);
        }
        return smartNoteId;
    }

    public void hideKeyboard(){
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
