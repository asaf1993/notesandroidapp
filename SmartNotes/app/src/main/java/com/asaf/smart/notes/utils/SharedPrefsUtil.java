package com.asaf.smart.notes.utils;


import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsUtil {

    Context context;
    SharedPreferences sharedPreferences;
    private final String LOG_TAG = "SharedPrefsUtil";

    public SharedPrefsUtil(Context ctx , String fileName){
        this.context = ctx;
        this.sharedPreferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    public String getStringValue(String key){
        if (checkIfKeyExists(key)) {
            return sharedPreferences.getString(key, "");
        }
        else {
            return "";
        }
    }

    public void setValue(String key, Object valueObject){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        boolean newVal= true;

        if(valueObject instanceof String)
        {
            editor.putString(key,(String) valueObject);
        }
        else if (valueObject instanceof Integer){
            editor.putInt(key,(Integer) valueObject);
        }
        else if (valueObject instanceof Boolean){
            editor.putBoolean(key, (Boolean) valueObject);
        }
        else {
            newVal = false;
        }
        if(newVal) {
            editor.apply();
        }
    }

    public void removeKeyValue(String key){

        SharedPreferences.Editor editor = sharedPreferences.edit();

        if(checkIfKeyExists(key)) {
            editor.remove(key);
            editor.apply();
        }

    }

    public boolean checkIfKeyExists(String key){
        return sharedPreferences.contains(key);
    }
}
