package com.asaf.smart.notes.utils;

import android.content.Context;
import android.graphics.Color;

public class ColorsUtil {

    Context context;
    String colorHexString;
    int colorInt;

    public ColorsUtil(Context ctx){
        this.context = ctx;
    }

    public ColorsUtil(Context ctx, int resourceColor){
        this.context = ctx;
        this.colorInt = resourceColor;
    }

    public String getColorHexString(int resource){
        return context.getResources().getString(resource);
    }

    public int getColorInt(String hexString){
        return Color.parseColor(hexString);
    }

    public int getColorHexInt(int resource){
        return getColorInt(getColorHexString(resource));
    }
}
