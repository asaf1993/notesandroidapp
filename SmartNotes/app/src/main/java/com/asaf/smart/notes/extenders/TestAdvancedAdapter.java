package com.asaf.smart.notes.extenders;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.asaf.smart.notes.NoteActivity;
import com.asaf.smart.notes.R;
import com.asaf.smart.notes.utils.ColorsUtil;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.LegacySwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableSwipeableItemViewHolder;


public class TestAdvancedAdapter extends RecyclerView.Adapter<TestAdvancedAdapter.SmartNoteHolder>
        implements DraggableItemAdapter<TestAdvancedAdapter.SmartNoteHolder>,
        LegacySwipeableItemAdapter<TestAdvancedAdapter.SmartNoteHolder> {

    private final ArrayList<SmartNote> dataList;
    private Context context;
    public final static String EXTRA_EDIT_NOTE = "com.asaf.smart.notes.extenders.adapter.edit";

    public TestAdvancedAdapter(ArrayList<SmartNote> smartNoteArrayList){
        this.dataList = smartNoteArrayList;
        setHasStableIds(true);
    }


    @Override
    public long getItemId(int position) {
        return dataList.get(position).getNoteId();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public SmartNoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        this.context = parent.getContext();
        View v = inflater.inflate(R.layout.activity_main_card_item_parent, parent, false);
        return new SmartNoteHolder(v);
    }

    @Override
    public void onBindViewHolder(SmartNoteHolder holder, int position) {

        final SmartNote item = dataList.get(position);
        ColorsUtil colorsUtil = new ColorsUtil(context);

        int mainIconColor = colorsUtil.getColorHexInt(R.color.color_generic_white);
        int smallIconsColor = colorsUtil.getColorHexInt(R.color.color_notes_CardItemSmallIcon);

        //Sets TextViews text
        holder.noteSubject.setText(item.noteSubject);
        holder.noteBody.setText(item.noteBody);

        //Icons image views and colors
        ImageView imageItem = holder.noteIconView;
        LinearLayout mainLinearLayout = holder.mainLayout;
        ImageView imageEdit = holder.noteEditIcon;
        ImageView imageReorder = holder.noteReorderIcon;
        imageItem.setColorFilter(mainIconColor);
        imageEdit.setColorFilter(smallIconsColor);
        imageReorder.setColorFilter(smallIconsColor);

        /* set swiping properties
        holder.setSwipeItemSlideAmount(
                item.isPinedToSwipeLeft() ? RecyclerViewSwipeManager.OUTSIDE_OF_THE_WINDOW_LEFT : 0);
        */

        //Click listeners
        imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NoteActivity.class);
                intent.putExtra(EXTRA_EDIT_NOTE, item.toString());
                context.startActivity(intent);

            }
        });

        mainLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Note Clicked, ID: " + item.noteId.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        Log.d("TestAdapterMove", "onMoveItem(fromPosition = " + fromPosition + ", toPosition = " + toPosition + ")");

        if (fromPosition == toPosition) {
            return;
        }
        moveItem(fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    private void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }
        final SmartNote item = dataList.remove(fromPosition);
        dataList.add(toPosition, item);
    }

    @Override
    public int onGetSwipeReactionType(SmartNoteHolder holder, int position, int x, int y) {
        return RecyclerViewSwipeManager.REACTION_CAN_SWIPE_RIGHT;
    }

    @Override
    public void onSetSwipeBackground(SmartNoteHolder holder, int position, int type) {
        int bgRes = 0;
        switch (type) {
            case RecyclerViewSwipeManager.DRAWABLE_SWIPE_NEUTRAL_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_neutral;
                break;
            case RecyclerViewSwipeManager.DRAWABLE_SWIPE_LEFT_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_left;
                break;
            case RecyclerViewSwipeManager.DRAWABLE_SWIPE_RIGHT_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_right;
                break;
        }
        holder.itemView.setBackgroundResource(bgRes);
    }

    @Override
    public int onSwipeItem(SmartNoteHolder holder, int position, int result) {
        switch (result) {
            // swipe right
            case RecyclerViewSwipeManager.RESULT_SWIPED_RIGHT:
                if (dataList.get(position).isPined) {
                    // pinned --- back to default position
                    return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_DEFAULT;
                } else {
                    // not pinned --- remove
                    return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_REMOVE_ITEM;
                }
                // swipe left -- pin
            case RecyclerViewSwipeManager.RESULT_SWIPED_LEFT:
                return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_MOVE_TO_SWIPED_DIRECTION;
            // other --- do nothing
            case RecyclerViewSwipeManager.RESULT_CANCELED:
            default:
                return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_DEFAULT;
        }
    }

    @Override
    public void onPerformAfterSwipeReaction(SmartNoteHolder holder, int position, int result, int reaction) {
        final SmartNote item = dataList.get(position);
        if (reaction == RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_REMOVE_ITEM) {
            //dataList.remove(position);
            //notifyItemRemoved(position);
        } else if (reaction == RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_MOVE_TO_SWIPED_DIRECTION) {
            item.isPined = true;
            //notifyItemChanged(position);

        } else {
            item.isPined = false;
            //item.setPinedToSwipeLeft(false);
        }
    }


    @Override
    public boolean onCheckCanStartDrag(SmartNoteHolder holder, int position, int x, int y) {
        // x, y --- relative from the itemView's top-left
        final View containerView = holder.noteView;
        final View dragHandleView = holder.noteReorderIcon;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(SmartNoteHolder holder, int position) {
        // no drag-sortable range specified
        return null;
    }


    public static class SmartNoteHolder extends AbstractDraggableSwipeableItemViewHolder{
        private View noteView;
        private ImageView noteIconView;
        private TextView noteSubject;
        private TextView noteBody;
        private ImageView noteEditIcon;
        private ImageView noteReorderIcon;
        private LinearLayout mainLayout;

        private SmartNoteHolder(View view){
            super(view);
            this.noteView = view;
            noteIconView = (ImageView) view.findViewById(R.id.icon_note);
            noteSubject = (TextView) view.findViewById(R.id.note_list_subject);
            noteBody = (TextView) view.findViewById(R.id.note_list_body);
            noteEditIcon = (ImageView) view.findViewById(R.id.icon_edit);
            noteReorderIcon = (ImageView) view.findViewById(R.id.icon_reorder);
            mainLayout = (LinearLayout) view.findViewById(R.id.main_touch_layout);
        }

        @Override
        public View getSwipeableContainerView() {
            return noteView;
        }
    }

    public static boolean hitTest(View v, int x, int y) {
        final int tx = (int) (ViewCompat.getTranslationX(v) + 0.5f);
        final int ty = (int) (ViewCompat.getTranslationY(v) + 0.5f);
        final int left = v.getLeft() + tx;
        final int right = v.getRight() + tx;
        final int top = v.getTop() + ty;
        final int bottom = v.getBottom() + ty;

        return (x >= left) && (x <= right) && (y >= top) && (y <= bottom);
    }
}
