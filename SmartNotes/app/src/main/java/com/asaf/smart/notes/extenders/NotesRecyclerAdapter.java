package com.asaf.smart.notes.extenders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asaf.smart.notes.NoteActivity;
import com.asaf.smart.notes.R;
import com.asaf.smart.notes.utils.ColorsUtil;

import java.util.ArrayList;


public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {

    public ArrayList<SmartNote> smartNoteArrayList;
    private Context context;
    final private String LOG_TAG = "NotesRecyclerAdapter";
    public final static String EXTRA_EDIT_NOTE = "com.asaf.smart.notes.extenders.adapter.edit";

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private View noteView;
        private ImageView noteIconView;
        private TextView noteSubject;
        private TextView noteBody;
        private ImageView noteEditIcon;
        private ImageView noteReorderIcon;
        private LinearLayout mainLayout;


        private ViewHolder(View view){
            super(view);
            this.noteView = view;
            noteIconView = (ImageView) view.findViewById(R.id.icon_note);
            noteSubject = (TextView) view.findViewById(R.id.note_list_subject);
            noteBody = (TextView) view.findViewById(R.id.note_list_body);
            noteEditIcon = (ImageView) view.findViewById(R.id.icon_edit);
            noteReorderIcon = (ImageView) view.findViewById(R.id.icon_reorder);
            mainLayout = (LinearLayout) view.findViewById(R.id.main_touch_layout);
        }
    }

        // Provide a suitable constructor (depends on the kind of dataset)
    public NotesRecyclerAdapter(ArrayList<SmartNote> data) {
        smartNoteArrayList = data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NotesRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        this.context = parent.getContext();
        View v = LayoutInflater.from(context)
                .inflate(R.layout.activity_main_card_item_parent, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final SmartNote smartNote = smartNoteArrayList.get(position);

        ColorsUtil colorsUtil = new ColorsUtil(context);

        int mainIconColor = colorsUtil.getColorHexInt(R.color.color_generic_white);
        int smallIconsColor = colorsUtil.getColorHexInt(R.color.color_notes_CardItemSmallIcon);

        //Sets TextViews text
        holder.noteSubject.setText(smartNote.noteSubject);
        holder.noteBody.setText(smartNote.noteBody);

        //Icons image views and colors
        ImageView imageItem = holder.noteIconView;
        LinearLayout mainLinearLayout = holder.mainLayout;
        ImageView imageEdit = holder.noteEditIcon;
        ImageView imageReorder = holder.noteReorderIcon;
        imageItem.setColorFilter(mainIconColor);
        imageEdit.setColorFilter(smallIconsColor);
        imageReorder.setColorFilter(smallIconsColor);

        //Click listeners
        imageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NoteActivity.class);
                intent.putExtra(EXTRA_EDIT_NOTE, smartNote.toString());
                context.startActivity(intent);

            }
        });

        mainLinearLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Note Clicked, ID: " + smartNote.noteId.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        imageReorder.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(context, "Reorder Long Clicked, ID: " + smartNote.noteId.toString(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return smartNoteArrayList.size();
    }

    public void setDataArrayList(ArrayList<SmartNote> smartNoteArrayList){
        this.smartNoteArrayList = smartNoteArrayList;
    }
}
