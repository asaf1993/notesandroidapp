package com.asaf.smart.notes.utils;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.asaf.smart.notes.extenders.SmartNote;
import com.asaf.smart.notes.props.EmailProps;
import com.asaf.smart.notes.props.SharedPrefsProps;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BackupUtil {

    private final String LOG_TAG = "BackupUtil";
    Context context;
    SharedPrefsUtil sharedPrefsUtil;

    public BackupUtil(Context ctx){
        context = ctx;
        sharedPrefsUtil = new SharedPrefsUtil(context, SharedPrefsProps.FILE_DEFAULT_PREFS);
    }

    private void SetBackupEmail(String userEmail){
        sharedPrefsUtil.setValue(SharedPrefsProps.KEY_USER_EMAIL, userEmail);
    }

    private String getBackupEmail(){
        return sharedPrefsUtil.getStringValue(SharedPrefsProps.KEY_USER_EMAIL);
    }

    public Boolean BackupNotes(){

        Log.d(LOG_TAG, "Email backup started");
        ValidateBackupEmail();

        FileUtil fileUtil = new FileUtil(context);

        EmailUtil emailUtil = new EmailUtil();
        String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        emailUtil.setToEmail(getBackupEmail());
        emailUtil.setSubject(dateStr);
        emailUtil.setBody("notes_backup.dat");

        String file = fileUtil.readFile(SmartNote.notesFile);
        emailUtil.addAttachment("notes_backup.dat", file);
        emailUtil.setTemplate(EmailProps.templateIdBackupNotes);

        return emailUtil.sendEmail();
    }

    private void ValidateBackupEmail(){

        if(!sharedPrefsUtil.checkIfKeyExists(SharedPrefsProps.KEY_USER_EMAIL)){
            SetBackupEmail("afalpi10@gmail.com");
        }
    }
}
