package com.asaf.smart.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.util.Log;
import android.widget.Toast;

import com.asaf.smart.notes.extenders.DragSortRecycler;
import com.asaf.smart.notes.extenders.NotesRecyclerAdapter;
import com.asaf.smart.notes.extenders.SmartNote;
import com.asaf.smart.notes.utils.BackupUtil;
import com.asaf.smart.notes.utils.FileUtil;

import java.util.ArrayList;


public class MainActivity extends Activity {

    private final static String LOG_TAG = "MainActivity";
    public final static String EXTRA_STRING = "com.asaf.smart.notes.NoteString";
    public ArrayList<SmartNote> smartNotesArrayList;
    private Context context;
    private NotesRecyclerAdapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        setContentView(R.layout.activity_main);


        //final ListView listView = (ListView) findViewById(R.id.notes_list);
        recyclerView = (RecyclerView) findViewById(R.id.notes_recycler_view);

        //Checks if notes file already exist
        if(FileUtil.doesFileExist(context, SmartNote.notesFile)){
            smartNotesArrayList = SmartNote.getSavedNotes(context);
        }
        else{
            smartNotesArrayList = new ArrayList<>();
        }

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        // specify an adapter (see also next example)
        adapter = new NotesRecyclerAdapter(smartNotesArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(null);


        //Drag sort recycler config and implementation
        //Docs: https://github.com/emileb/DragSortRecycler
        DragSortRecycler dragSortRecycler = new DragSortRecycler();
        dragSortRecycler.setViewHandleId(R.id.icon_reorder);
        dragSortRecycler.setFloatingAlpha(0.4f);
        dragSortRecycler.setFloatingBgColor(0xffff80);
        dragSortRecycler.setAutoScrollSpeed(0.3f);
        dragSortRecycler.setAutoScrollWindow(0.1f);

        dragSortRecycler.setOnItemMovedListener(new DragSortRecycler.OnItemMovedListener() {
            @Override
            public void onItemMoved(int from, int to) {
                //Log.d(LOG_TAG, "Moved from: " + from + " to: " + to);

                if (from != to) {
                    Toast.makeText(context, "Moved from: " + from + " to: " + to, Toast.LENGTH_SHORT).show();
                    //Update notes order in the activity
                    adapter.notifyItemMoved(from, to);

                    SmartNote movedNote = adapter.smartNoteArrayList.get(from);
                    SmartNote replacedNote = adapter.smartNoteArrayList.get(to);

                    adapter.smartNoteArrayList.set(to, movedNote);
                    adapter.smartNoteArrayList.set(from, replacedNote);
                    //Update notes order in the data files
                    SmartNote.setNewNotes(context, adapter.smartNoteArrayList);
                }
            }
        });

        recyclerView.addItemDecoration(dragSortRecycler);
        recyclerView.addOnItemTouchListener(dragSortRecycler);
        recyclerView.setOnScrollListener(dragSortRecycler.getScrollListener());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause(){
        // Clear the entered text in the edit text box
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();

        updateNotesList();
        //TODO: add resume display new notes in main activity
    }

    /** Called when the user clicks the + button */
    public void startNewNote(MenuItem item){
        // Do something in response to button
        Intent intent = new Intent(this, NoteActivity.class);
        startActivity(intent);
    }

    /** Called when the user clicks the backup notes in the menu */
    public void backupNotes(MenuItem item){
        // Do something in response to the menu item
        context = getApplicationContext();
        BackupUtil util = new BackupUtil(context);

        if(util.BackupNotes()) {
            Toast.makeText(context, "Sending backup email...", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "Failed to send backup email", Toast.LENGTH_LONG).show();
        }
    }

    /** Called when the user clicks the Send button */
    public void testActivity(View view) {
        // Do something in response to button
        context = getApplicationContext();
        //Toast.makeText(context, "Button not set ATM!", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, TestActivity.class);
        startActivity(intent);
    }

    public void updateNotesList(){
        context = getApplicationContext();
        adapter.setDataArrayList(SmartNote.getSavedNotes(context));
        adapter.notifyDataSetChanged();
    }
}
