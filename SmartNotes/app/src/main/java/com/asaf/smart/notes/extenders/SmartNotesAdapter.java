package com.asaf.smart.notes.extenders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asaf.smart.notes.R;

import java.util.ArrayList;


public class SmartNotesAdapter extends ArrayAdapter<SmartNote> {

    Context context;
    int itemLayoutResourceId;
    final private String LOG_TAG = "SmartNotesAdapter";
    public ArrayList<SmartNote> smartNoteArrayList;

    public SmartNotesAdapter(Context context, int itemLayoutResourceId, ArrayList<SmartNote> smartNoteArrayList){

        super(context, itemLayoutResourceId, smartNoteArrayList);

        this.itemLayoutResourceId = itemLayoutResourceId;
        this.context = context;
        this.smartNoteArrayList = smartNoteArrayList;
    }

    @Override
    public int getCount() {
        return smartNoteArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemView = convertView;
        final SmartNoteHolder smartNoteHolder;

        if (itemView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            itemView = inflater.inflate(itemLayoutResourceId, parent, false);
            smartNoteHolder = new SmartNoteHolder(itemView);
            itemView.setTag(smartNoteHolder);
        }
        else
        {
            smartNoteHolder = (SmartNoteHolder)itemView.getTag();
        }

        final SmartNote smartNote = smartNoteArrayList.get(position);

        String subjectString = smartNote.noteSubject;
        smartNoteHolder.noteSubject.setText(subjectString); //TODO: missing subject

        String bodyString = smartNote.noteBody;
        String bodyStringSuffix = bodyString.substring(0, (30 < (bodyString.length() - 1) ? 30 : (bodyString.length())));
        smartNoteHolder.noteBody.setText(bodyStringSuffix.split("\\r?\\n")[0] + "...");

        ImageView imageEdit = smartNoteHolder.noteEditIcon;
        imageEdit.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(context, "Edit Clicked, ID: " + smartNote.noteId.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        ImageView imageReorder = smartNoteHolder.noteReorderIcon;
        imageReorder.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(context, "Reorder Long Clicked, ID: " + smartNote.noteId.toString(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        return itemView;
    }

    public static class SmartNoteHolder{
        private View noteView;
        private ImageView noteIconView;
        private TextView noteSubject;
        private TextView noteBody;
        private ImageView noteEditIcon;
        private ImageView noteReorderIcon;

        private SmartNoteHolder(View view){
            this.noteView = view;
            noteIconView = (ImageView) view.findViewById(R.id.icon_note);
            noteSubject = (TextView) view.findViewById(R.id.note_list_subject);
            noteBody = (TextView) view.findViewById(R.id.note_list_body);
            noteEditIcon = (ImageView) view.findViewById(R.id.icon_edit);
            noteReorderIcon = (ImageView) view.findViewById(R.id.icon_reorder);
        }
    }

}
