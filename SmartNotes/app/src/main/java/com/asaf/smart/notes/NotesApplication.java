package com.asaf.smart.notes;

import android.app.Application;
import android.util.Log;

import com.asaf.smart.notes.props.EmailProps;
import com.asaf.smart.notes.utils.EmailUtil;

import java.io.PrintWriter;
import java.io.StringWriter;


public class NotesApplication extends Application{

    @Override
    public void onCreate()
    {
        super.onCreate();
        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException (Thread thread, Throwable e)
    {

        String exceptionStr = e.toString();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw); // not all Android versions will print the stack trace automatically
        String stackTraceString = sw.toString();

        Log.d("UHE", exceptionStr);
        Log.d("UHE", "StackTrace: " + stackTraceString);

        EmailUtil emailUtil = new EmailUtil();
        emailUtil.setToEmail(EmailProps.mailToSupport);
        emailUtil.setSubject(exceptionStr);
        emailUtil.setBody(stackTraceString);
        emailUtil.setTemplate(EmailProps.templateIdAppCrush);

        emailUtil.sendEmail();

        System.exit(1); // kill off the crashed app
    }
}
