package com.asaf.smart.notes.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonUtil {

    private JSONObject jsonObject = new JSONObject();
    private static final String LOG_TAG = "JsonHandler";

    //Constructors
    public JsonUtil(){
    }

    public JsonUtil(String jsonString){

        try {
            jsonObject = new JSONObject(jsonString);
        }
        catch (JSONException e) {
            Log.d(LOG_TAG, e.toString());
        }
    }

    //Methods
    public JSONObject getJsonObject(){
        return jsonObject;
    }

    public String getJsonToString(){
        return jsonObject.toString();
    }

    public Integer getJsonIntValueByKey(String key){

        Integer integer;

        try {
            integer = jsonObject.getInt(key);
        }
        catch (JSONException e) {
            integer = 0;
            Log.d(LOG_TAG, e.toString());
        }

        return integer;
    }

    public String getJsonStringValueByKey(String key){

        String string;

        try {
            string = jsonObject.getString(key);
        }
        catch (JSONException e) {
            string = "";
            Log.d(LOG_TAG, e.toString());
        }

        return string;
    }

    /*
    public Date getJsonDateValueByKey(String key){

        String tempDate;
        Date date;

        tempDate = getJsonStringValueByKey(key);
        DateFormat format = new SimpleDateFormat()

    }
    */

    public void addToJson(String key, Object obj){

        try{
            jsonObject.put(key, obj);
        }
        catch (JSONException e) {
            Log.d(LOG_TAG, e.toString());
        }

    }
}
