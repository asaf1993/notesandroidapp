package com.asaf.smart.notes.utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class FileUtil {

    Context context;
    private static File filesPath;
    private static String FILE_SEPERATOR = "";
    private static final String LOG_TAG = "FileHandler";
    private static final String UTF8 = "utf8";
    private static final int BUFFER_SIZE = 8192;


    public FileUtil(Context ctx){
        context = ctx;
        Log.d(LOG_TAG, ctx.getFilesDir().toString());
        filesPath = ctx.getFilesDir();
    }

    public static boolean doesFileExist(Context ctx, String fileName){
        File file = new File(ctx.getFilesDir() + FileUtil.FILE_SEPERATOR + fileName);
        return file.exists();
    }

    public File getFile(String fileName){
        return new File(filesPath + FileUtil.FILE_SEPERATOR + fileName);
    }


    public void writeFile(String fileName, String writeString){

        String fullFilePath = filesPath + FILE_SEPERATOR + fileName;

        try {

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fullFilePath), UTF8),BUFFER_SIZE);
            bw.write(writeString);
            bw.close();

            //Log.d(LOG_TAG, "Wrote: " + writeString);
        }
        catch (IOException e) {
            Log.e(LOG_TAG, "File write failed: " + e.toString());
        }

    }
    
    public void writeFileLines(String fileName, ArrayList<String> fileLinesArrayList){

        String writeString = "";

        for (String lineString : fileLinesArrayList){
            writeString += lineString + "\n";
        }

        writeFile(fileName, writeString);
    }

    public String readFile(String fileName){

        int readInt;
        String buffer = "";
        String fileText = "";
        String fullFilePath = filesPath + FILE_SEPERATOR + fileName;

        createFileIfNotExists(fileName);

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fullFilePath), UTF8),BUFFER_SIZE);

            while( (readInt=br.read()) != -1){
                buffer += Character.toString((char)readInt);
            }

            fileText += buffer;

            br.close();
            //Log.d(LOG_TAG, "File text: " + fileText);
        }
        catch (IOException e) {
            Log.e(LOG_TAG, "File read failed: " + e.toString());
        }

        return fileText;
    }

    public ArrayList<String> readFileLines(String fileName){

        String fileText = readFile(fileName);

        if (fileText.isEmpty()) {
            return  new ArrayList<>();
        }
        return new ArrayList<>(Arrays.asList(fileText.split("\\r?\\n")));
    }

    private void createFileIfNotExists(String fileName){

        context.getFilesDir();
        File file = new File(filesPath + FileUtil.FILE_SEPERATOR + fileName);

        try {
            if (!file.exists()) {
                Log.d(LOG_TAG, "File does not exist, creating file.");
                file.createNewFile();
            }
        }
        catch (IOException e){
            Log.d(LOG_TAG, e.toString());
        }
    }
}
